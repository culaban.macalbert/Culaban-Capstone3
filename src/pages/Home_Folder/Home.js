import { Fragment } from "react";
import Footer from "../../components/Footer_Folder/Footer";
import AppNavbar from "../../components/AppNavbar_Folder/AppNavbar"
import Newsletter from "../../components/NewsLetter";
import Header from "../../components/Header_Folder/Headers";
import { Container } from "react-bootstrap";
import Announcement from "../../components/Announcement";
import ProductAds from "../../components/ProductAds_Folder/productAds";
import Banner from "../../components/Banner_Folder/Banner";

export default function Home() {
    return (

    
        <Fragment>

            <Header/>
            <Announcement/>
            <Banner/>
            <ProductAds/>
            <Newsletter/>
            <Footer/>
            
        </Fragment>
    

    )
}